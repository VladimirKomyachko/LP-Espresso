
    // dummy data
    var products = [
        {
            'name': 'De Longhi EXPERT',
            'price': '1111',
            'img': [
                {
                    'src': './img/delongi.jpg',
                    'color': 'white'
                },
                {
                    'src': './img/ekspres3.jpg',
                    'color': 'grey'
                },
                {
                    'src': './img/ekspres3.jpg',
                    'color': 'orange'
                }
            ],
            'colors': ['white', 'grey', 'orange'],
            'hasMilk': 'milk',
            'badge': 'badge'
        },
        {
            'name': 'Krups Inissia Biały XN100110',
            'price': '333',
            'img': [
                {
                    'src': './img/ekspres3.jpg',
                    'color': 'white'
                },
                {
                    'src': './img/ekspres3.jpg',
                    'color': 'red'
                }
            ],
            'colors': ['white', 'red'],
            'hasMilk': 'noMilk',
            'badge': 'badge'
        },
        {
            'name': 'De Longhi Lattissima Czarny EN550.B',
            'price': '2111',
            'img': [
                {
                    'src': './img/ekspresM3.jpg',
                    'color': 'black'
                },
                {
                    'src': './img/ekspresM3.jpg',
                    'color': 'yellow'
                }
            ],
            'colors': ['black', 'yellow'],
            'hasMilk': 'milk',
            'badge': 'badge2'
        },
        {
            'name': 'De Longhi EXPERT',
            'price': '1112',
            'img': [
                {
                    'src': './img/delongi.jpg',
                    'color': 'white'
                },
                {
                    'src': './img/delongi.jpg',
                    'color': 'grey'
                },
                {
                    'src': './img/delongi.jpg',
                    'color': 'orange'
                }
            ],
            'colors': ['white', 'grey', 'orange'],
            'hasMilk': 'milk',
            'badge': 'badge'
        },
        {
            'name': 'De Longhi EXPERT',
            'price': '1112',
            'img': [
                {
                    'src': './img/delongi.jpg',
                    'color': 'white'
                },
                {
                    'src': './img/delongi.jpg',
                    'color': 'grey'
                },
                {
                    'src': './img/delongi.jpg',
                    'color': 'orange'
                }
            ],
            'colors': ['white', 'grey', 'orange'],
            'hasMilk': 'noMilk',
            'badge': 'badge'
        },
        {
            'name': 'De Longhi EXPERT',
            'price': '1112',
            'img': [
                {
                    'src': './img/delongi.jpg',
                    'color': 'red'
                },
                {
                    'src': './img/delongi.jpg',
                    'color': 'grey'
                },
                {
                    'src': './img/delongi.jpg',
                    'color': 'orange'
                }
            ],
            'colors': ['red', 'grey', 'orange'],
            'hasMilk': 'milk',
            'badge': 'badge'
        }
    ];
    var filtredProd = products.slice();
    var prod_slider = $('.prod_slider');
    var activeFilters = {color: '', milk: ''};
    var itemsToShow = products.length;
    var sort = {type: 'name', order: 'order'};

    // slider init after DOM ready
    addProdItems();

    $(document).ready(function () {

          $('#switchState').on('click', function (e) {
              e.preventDefault();

              itemsToShow = products.length;
              addProdItems();

              if ($(this).hasClass('sliderState')) {
                  $(this).removeClass('sliderState');
                  $(this).text('Schowaj wszystkie');
                  destroyOwl();
              } else {
                  $(this).addClass('sliderState');
                  $(this).text('POKAŻ WSZYSTKIE');
                  initSlider();
              }
              return false;
          });

          // filters
          // dropdown menu
          $('.dropdown_menu').on('click', function () {
              if ($(this).hasClass('open')) {
                  $(this).removeClass('open');
              } else {
                  $(this).addClass('open');
              }
              return false;
          });

          // filterMilk
          $('#filterMilk .dropdown_list li a').on('click', function (e) {
              e.preventDefault();
              activeFilters.milk = $(this).data('filter');
              $('#filterMilk .selected_option').text($(this).text());
              addProdItems();
          });

          // filter colors
          $('.filter_box .color_list li').on('click', function (e) {
              e.preventDefault();
              activeFilters.color = $(this).data('filter');
              addProdItems();
          });
          // filter > <
          $('#lessMore .dropdown_list li a').on('click', function (e) {
              e.preventDefault();
              sort.order = $(this).data('sort');
              sort.type = $(this).data('filter');
              $('#lessMore > .selected_option').text($(this).text());
              addProdItems();
          });
          // colorimg
          $('body').on('click', ' .color_list .circle', function () {
              var val = $(this).data('filter');
              var prodImg = $(this).parent().parent().parent().parent().find('.prod-img > img');
              var color_list = $(this).parent().children();
              color_list.each(function (el) {
                if($(this).hasClass(val)){
                  $(this).addClass('active');
                }else {
                    $(this).removeClass('active');
                }
              })
              $(this).addClass('active');
              prodImg.each(function (el) {
                  if ($(this).hasClass(val)) {
                      $(this).addClass('active');
                  } else {
                      $(this).removeClass('active');
                  }
              })

          });

    });


  // methods
    function filterProduct() {

        filtredProd = products.slice();
        filtredProd = filtredProd.filter(function (item) {
            var milk = activeFilters.milk !== ''
                ? item.hasMilk === activeFilters.milk
                : true;
            var color = activeFilters.color !== ''
                ? item.colors.includes(activeFilters.color)
                : true;
            return milk && color;
        });
    }
    function sortProduct() {

        //check order or reverse list
        var order = sort.order === 'order'? 1 : -1;
        if (sort.type == 'price') {
            filtredProd = filtredProd.sort(function (a, b) {
                return parseFloat(a[sort.type]) > parseFloat(b[sort.type])?order: - order;
            });
        }
        if (sort.type  == 'name') {
            filtredProd = filtredProd.sort(function (a, b) {
                return a[sort.type] > b[sort.type]?order: - order  ;
            });
        }
    }
    //  create products to show
    function addProdItems() {
        prod_slider.addClass('space');
        prod_slider.html('');
        filterProduct();
        sortProduct();

        var createdProducts = filtredProd.map(function (product, index) {
            if(index > itemsToShow -1)
                return null;
            var item = '<div class="prod_item">';
            item +=
                '<div class="prod_wrap">' +
                '<div class="prod-img">';

            product.img.forEach(function (image, index) {
                if (index === 0) {
                    item += '<img class="active ' + image.color + '" src="' + image.src + '"  alt="">';
                } else {
                    item += '<img class="' + image.color + '" src="' + image.src + '" data-filter="' + image.color + '" alt="">';
                }
            });
            item +=
                '</div>' +
                '<div class="' + product.badge + '"></div>' +
                '<div class="prod_info">' +
                '<span class="prod_name">' + product.name + '</span>' +
                '<span class="prod_price">Cena:' + product.price + 'zł</span>' +
                '<div class="color_menu">' +
                '<ul class="color_list">';
            product.img.forEach(function (image, i) {
                if(i === 0){
                    item += '<li class="circle active ' + image.color + '" data-filter="' + image.color + '"></li>';
                }else{
                  item += '<li class="circle ' + image.color + '" data-filter="' + image.color + '"></li>';
                }
            });
            item +=
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            return item;
        });


        prod_slider.append(createdProducts.join(''));
        // destroyOwl('.prod_slider');
        // initSlider();
        if($('#switchState').hasClass('sliderState')){
          destroyOwl();
          initSlider();
        }

    }


    function initSlider() {
        prod_slider.addClass('owl-carousel');
        prod_slider.removeClass('space');
        $('.prod_slider').owlCarousel({
            loop: true,
            slideBy: 3,
            nav: true,
            navText: ['', ''],
            autoplayHoverPause: true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1,
                    slideBy: 1,
                },
                // breakpoint from 768 up
                768: {
                    items: 2,
                    slideBy: 2,
                },
                996: {
                    items: 3,
                    slideBy: 3,
                }
            }
        });
    }

    function destroyOwl() {
        prod_slider.addClass('space');
        prod_slider.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
        prod_slider.find('.owl-stage-outer').children().unwrap();
    }
